import numpy as np
import matplotlib.pyplot as plt


# DONNÉES
f_Hz = 80
R_Ohm = 20e3
C_F = 50e-9
E_max_V = 1
E_min_V = 0

T_s = 1/f_Hz
T_ms = T_s * 1e3

t_start_ms = 0
t_stop_ms = 3 * T_ms

tau_s = R_Ohm * C_F
tau_ms = tau_s * 1e3


def E_V(t_ms):
    half_period_ms = T_ms / 2
    number_half_periods = int(t_ms / half_period_ms)
    return E_max_V if number_half_periods % 2 == 0 else E_min_V


def next_uC_V(u_C_V, E_V, dt_ms):
    du_C_V = dt_ms * (E_V - u_C_V) / tau_ms
    return u_C_V + du_C_V


dt_ms = 0.01

t_ms = t_start_ms
u_C_V = 0

t_ms_list = []
u_C_V_list = []
E_V_list = []

while t_ms <= t_stop_ms:
    t_ms_list.append(t_ms)

    u_C_V = next_uC_V(u_C_V, E_V(t_ms), dt_ms)
    u_C_V_list.append(u_C_V)
    E_V_list.append(E_V(t_ms))

    t_ms += dt_ms


plt.plot(
    t_ms_list, u_C_V_list,
    label="$u_C$",
    color="blue"
)

plt.plot(
    t_ms_list, E_V_list,
    label="$E$",
    color="orange", linestyle="dashed"
)

plt.axis([
    t_start_ms, t_stop_ms,
    E_min_V, E_max_V + 0.2
])

plt.xlabel("temps (ms)")
plt.ylabel("tension (V)")

plt.title("Circuit $R C$ série")
plt.legend()
plt.show()