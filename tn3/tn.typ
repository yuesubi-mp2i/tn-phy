#set page(
    header: [Physique -- Travail Numérique 3 #h(1fr)
        #smallcaps[Lux] Matthieu MP2I],
    numbering: "— 1/1 —"
)
// #set text(size: 14pt)

#show figure.caption: it => [
    #smallcaps[#it.supplement #it.counter.display(it.numbering)] --
    #it.body
]

#set heading(numbering: "1.1")

#show heading: it => block(
    if it.level == 2 {
        numbering(
            it.numbering,
            ..counter(heading)
                .at(it.location())
                .slice(1)
        ) + [. ]
    } + it.body
)


#align(center, [
    = TN3 -- Dynamique d'un système linéaire du premier ordre
])

#let res(formula) = rect($formula$, stroke: blue)
#let bigres(formula) = rect($ formula $, stroke: rgb("#000099"))

#rect(width: 100%, height: 13em)[
    #underline[Commentaire] :
]


== But
Le but de ce TN est d'utiliser l'algorithme d'Euler pour
approximer numériquement la solution d'une équation différencielle.

== Le problème physique

On considère le circuit $R C$ série :

#table(
  columns: 2,
  stroke: none,

  figure(
    image(
      width: 18em,
      "res/rc_serie.png"
    ),
    caption: [Circuit $R C$ série]
  ),
  [
    On a les égalités suivantes
    + loi des mailles : $E = u_R + u_C $
    + loi d'Ohm :
      $u_R = R i $
    + caractéristique du condensateur :
      $i = C (dif u_C)/(dif t) $

    Ainsi
    $ E = u_R + u_C 
      <=>^2 E = R i + u_C \
      <=>^3 E = R C (dif u_C)/(dif t) + u_C $
  ]
)

On obtient alors une expression que l'on peut utiliser pour 
l'algorithme de d'Euler :
$ bigres((E - u_C)/tau = (dif u_C)/(dif t))
  "où" tau = R C $


== Une simulation avec l'algorithme d'Euler
En appliquant l'algorithme d'Euler, on obtient le résulta suivant :

#figure(
  image(width: 16em, "res/result.png"),
  caption: [Résultat final, `LuxMatthieuTN03.py`]
)

#align(center, smallcaps[Fin $floral$])