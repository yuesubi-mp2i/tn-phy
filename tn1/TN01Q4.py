import numpy as np
import matplotlib. pyplot as plt


GLASS_REFR_INDEX = 1.5
AIR_REFR_INDEX = 1.0

RAY_INTERVAL = 0.3
RAY_AMOUNT = 10


def lens_coords():
    # Tableau de valeurs de pi/2 à -pi/2
    t = np.linspace(np.pi/2, -np.pi/2, 100)

    # Arc de cercle de la lentille
    xs = np.cos(t)
    ys = np.sin(t)

    # Ajout de la face droite de la lentille
    xs = np.append(xs, [0.0])
    ys = np.append(ys, [1.0])

    return xs, ys


def ray_cords():
    # Hauteur du rayon aléatoire
    y = np.random.rand() * 2.0 * RAY_INTERVAL - RAY_INTERVAL

    # Déterminer l'abscisse de contact
    x_contact_sqr = 1 - y*y
    x_contact = np.sqrt(1 - y*y) if x_contact_sqr >= 0.0 else -2.0

    xs = [-1.0, x_contact]
    ys = [y, y]

    i_angle = np.arctan(y / x_contact) if x_contact != 0.0 else np.pi/2
    sin_r = GLASS_REFR_INDEX/AIR_REFR_INDEX * np.sin(i_angle)

    if abs(sin_r) <= 1.0:
        r_angle = np.arcsin(GLASS_REFR_INDEX/AIR_REFR_INDEX * np.sin(i_angle))

        result_angle = r_angle - i_angle

        xs.append(x_contact + 3.0*np.cos(result_angle))
        ys.append(y - 3.0*np.sin(result_angle))

    # Extrémités du rayon
    return np.array(xs), np.array(ys)


def find_optic_axis_intersection_with(x1, y1, x2, y2):
    m = (y2 - y1) / (x2 - x1)
    p = y1 - m * x1

    x = -p/m
    y = m * x + p
    return x, y


optic_axis_xs = np.array([-10, 10])
optic_axis_ys = np.array([0, 0])
plt.plot(optic_axis_xs, optic_axis_ys, label="axe optique", color="black")

average_intersection_x = 0.0
intersection_count = 0

for _ in range(RAY_AMOUNT):
    ray_xs, ray_ys = ray_cords()
    plt.plot(ray_xs, ray_ys, color="purple", linewidth=1)

    # Vérifier que le rayon a bien été réfracté
    if len(ray_xs) > 2:
        x, y = find_optic_axis_intersection_with(
            ray_xs[-2], ray_ys[-2], ray_xs[-1], ray_ys[-1]
        )
        average_intersection_x += x

        intersection_count += 1

lens_xs, lens_ys = lens_coords()
plt.plot(lens_xs, lens_ys, label="lentille", color="red")

if intersection_count != 0:
    average_intersection_x /= intersection_count
    plt.scatter([average_intersection_x], [0.0],
        marker="x", label="foyer image")

plt.axis("equal")
plt.axis([-2, 4, -2.0, 2.0])

plt.title("Détermination du foyer image")
plt.xlabel("x(m)")
plt.ylabel("y(m)")

plt.legend()
plt.show()