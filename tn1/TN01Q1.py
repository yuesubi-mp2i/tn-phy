import numpy as np
import matplotlib. pyplot as plt


def lens_coords():
    # Tableau de valeurs de pi/2 à -pi/2
    t = np.linspace(np.pi/2, -np.pi/2, 100)

    # Arc de cercle de la lentille
    xs = np.cos(t)
    ys = np.sin(t)

    # Ajout de la face droite de la lentille
    xs = np.append(xs, [0.0])
    ys = np.append(ys, [1.0])

    return xs, ys


lens_xs, lens_ys = lens_coords()
plt.plot(lens_xs, lens_ys, label="lentille", color="red")

optic_axis_xs = np.array([-10, 10])
optic_axis_ys = np.array([0, 0])
plt.plot(optic_axis_xs, optic_axis_ys, label="axe optique", color="black")

# Rapport des échelles x/y conservé
plt.axis("equal")

# Domaine des axes
plt.axis([-2, 4, -2.0, 2.0])

plt.title("Lentille demi-boule")
plt.xlabel("x(m)")
plt.ylabel("y(m)")

plt.legend()
plt.show()
