#import "@preview/cetz:0.1.2"

#set page(
    header: [Physique -- Travaux Numériques 1 #h(1fr)
        #smallcaps[Lux] Matthieu MP2I],
    numbering: "— 1/1 —"
)
#set text(size: 14pt)

#show figure.caption: it => [
    #smallcaps[#it.supplement #it.counter.display(it.numbering)] --
    #it.body
]

#set heading(numbering: "1.1")

#show heading: it => block(
    if it.level == 2 {
        numbering(
            it.numbering,
            ..counter(heading)
                .at(it.location())
                .slice(1)
        ) + [. ]
    } + it.body
)


#align(center, [
    = TN1 --  Réfraction : lentille demi-boule
])


== Présenter une courbe
Le script `TN01Q1.py` dessine une lentille demi-boule sur un graphe matplotlib
joint (@half_ball_lens).
La lentille est composée d'une centaine de points situés sur la partie en arc de
cercle, et d'un point qui relie les deux bouts.
L'axe optique est lui composé de deux points.

#figure(
    image("res/half_ball_lens.png"),
    caption: [Graphique généré par `TN01Q1.py`],
) <half_ball_lens>


== Utiliser un générateur de nombres aléatoires
On veut générer 10 rayons aléatoires pour chercher la position du foyer image
de la lentille.
Mais il faut tout de même que les rayons arrivent parrallèles avec l'axe
optique pour que les rayons qui sortent de la lentille se croissent au foyer
image.
On utilise la ligne suivante pour générer la position sur l'axe
des ordonnées d'un rayon :
```py
y = np.random.rand() * 2.0 * RAY_INTERVAL - RAY_INTERVAL
```
La fontion `np.random.rand` renvoie un flotant dans l'intervalle $[0, 1[$
et on se débrouille ensuite pour qu'il se trouve dans un intervalle de
demi-largeur $h_"ray int"$ centré en $0$.
On prendra $h_"ray ing"$ assez petit pour avoir des rayons paraxiaux, et donc
être dans le conditions de Gauss.

La @ten_rays montre un exemple de rayons générés avec $h_"ray ing" = 1$.

#figure(
    image("res/ten_rays.png"),
    caption: [Graphique généré par `TN01Q2.py`]
) <ten_rays>


== Réfraction par une lentille demi-boule

#figure(
    cetz.canvas(length: 8em, {
        import cetz.draw: *

        // axes
        line((-0.75, 0), (3, 0), mark: (end: ">"))
        content((), $ x $, anchor: "left")
        line((0, -0.1), (0, 1.5), mark: (end: ">"))
        content((), $ y $, anchor: "bottom")

        // lens
        line((0, 1), (0, -0.1), stroke: (paint: red, thickness: 2pt))
        arc((0, 1), start: 90deg, stop: -5deg, stroke: (paint: red, thickness: 2pt))

        let sqrt2over2 = calc.sqrt(2) / 2.0

        set-style(angle: (radius: 0.3, label-radius: .4,
            fill: red.lighten(80%),
            stroke: (paint: red.darken(50%))
        ))
        cetz.angle.angle(
            (sqrt2over2, sqrt2over2),
            (0, 0), (0, sqrt2over2),
            label: $theta_i$,
            mark: (start: ">", size: 0.1)
        )
        cetz.angle.angle(
            (0, 0), (1, 0), (1, 1),
            label: $theta_i$,
            mark: (start: ">", size: 0.1)
        )

        set-style(angle: (radius: 0.6, label-radius: .9,
            fill: green.lighten(80%),
            stroke: (paint: green.darken(50%))
        ))
        cetz.angle.angle(
            (sqrt2over2, sqrt2over2),
            (1, sqrt2over2),
            (2, 0),
            label: $theta_r - theta_i$,
            mark: (start: ">", size: 0.1)
        )
        line((sqrt2over2, sqrt2over2),
            (sqrt2over2 + 0.8, sqrt2over2),
            stroke: (paint: green, dash: "dashed"))

        set-style(angle: (radius: 0.3, label-radius: .4,
            fill: orange.lighten(80%),
            stroke: (paint: orange.darken(50%))
        ))
        cetz.angle.angle(
            (sqrt2over2, sqrt2over2),
            (2, 0), (1, 1),
            label: $theta_r$,
            mark: (start: ">", size: 0.1)
        )

        line((-0.1, -0.1),
            (sqrt2over2 + 0.35, sqrt2over2 + 0.35),
            stroke: (paint: purple, dash: "dashed"))
        
        line((-0.5, sqrt2over2), (sqrt2over2, sqrt2over2),
            stroke: (paint: purple))
        line((sqrt2over2, sqrt2over2), (2, 0),
            stroke: (paint: purple))

        line((sqrt2over2, -0.05), (sqrt2over2, 0.05), label: "xc")
        content(((sqrt2over2, 0.15), 0, (sqrt2over2, 0.15)), $x_c$)
    }),
    caption: [Schéma de la situation]
) <schema>

On veut déterminer le chemin d'un rayon dans la lentille.
Comme le rayon que l'on suit arrive permenticulairement à la face plane
de la lentille, le rayon réfracté est dans le plan et n'est pas dévié (selon
la loi de la réfraction de Snell-Descartes).

On commence par déterminer l'abscisse de contact $x_c$ du rayon,
$ 1 = x_c^2 + y^2 <==> x_c = sqrt(1 - y^2) $
On peut ensuite trouver l'angle incident $theta_i$ à l'aide
de la trigonométrie,
$ theta_i = "Arctan"(x_c / y) $
Et l'angle réfracté $theta_r$, grâce à la loi de la réfraction de
Snell-Descartes,
$ n_"verre" sin(theta_i) = n_"air" sin(theta_r)
    <==> theta_r = "Arcsin"(n_"verre"/n_"air" sin(theta_i)) $

On abouti alors à la @refrac_ray.

#figure(
    image("res/refrac_ray.png"),
    caption: [Graphique généré par `TN01Q3.py`]
) <refrac_ray>


== Conclusion
On cherche à trouver le foyer principal image de la lentille.
On sait que les foyers se situent sur l'axe optique, donc on cherche
l'intersection des rayons émergents de la lentille et de l'axe optique.

Grâce au calcul suivant, on trouve l'intersection d'un des rayons avec l'axe.
$ cases(y = m x + p, y = 0) <==> cases(x = -p/m, y = 0) $
Il ne manque plus qu'a faire une moyenne des différentes abscisse trouvées
et d'afficher le foyer image sur le graphique.

On obtient alors la @final.

#figure(
    image("res/final.png"),
    caption: [Graphique généré par `TN01Q4.py`]
) <final>

#align(center, smallcaps[Fin])