import numpy as np
import matplotlib. pyplot as plt


RAY_INTERVAL = 1.0 


for i in range(10):
    # Hauteur du rayon aléatoire
    y = np.random.rand() * 2.0 * RAY_INTERVAL - RAY_INTERVAL

    # Extrémités du rayon
    xs = np.array([-1, 3])
    ys = np.array([y, y])

    plt.plot(xs, ys, color="purple")

plt.axis("equal")
plt.axis([-2, 4, -2.0, 2.0])

plt.title("Rayons aléatoires")
plt.xlabel("x(m)")
plt.ylabel("y(m)")
plt.show()