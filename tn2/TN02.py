from math import sqrt, pi

import numpy as np
import numpy.random as rd
import matplotlib.pyplot as plt


# Constantes de conversions d'unités
us_to_s = 1e-6
mL_to_m3 = 1e-6
cm_to_m = 1e-2


# Mesures de l'énoncé
mes_R_ohm = 5.0e5
mes_dmin_m = 3.6 * cm_to_m
mes_dmax_m = 4.0 * cm_to_m

mes_tau1_s = np.array([48, 52, 56, 60, 62,
                        64, 68, 70, 74, 78,
                        82, 84, 88]) * us_to_s

mes_tau2_s = np.array([56, 62, 62, 68, 72,
                        76, 78, 84, 85, 94,
                        98, 99, 107]) * us_to_s

mes_V_m3    = np.array([50, 66, 78, 96, 106,
                        126, 146, 156, 176, 196,
                        212, 230, 250]) * mL_to_m3


# Calcul des valeurs et des incertitudes types
R_ohm = mes_R_ohm
u_R = R_ohm * 0.01

d_m = (mes_dmax_m + mes_dmin_m) / 2.0
u_d = (mes_dmax_m - mes_dmin_m) / (2.0 * sqrt(3))

tau_s = (mes_tau2_s + mes_tau1_s) / 2.0
u_tau = (mes_tau2_s - mes_tau1_s) / (2.0 * sqrt(3))

V_m3 = mes_V_m3
u_V = (4 * mL_to_m3) / (2.0 * sqrt(3))

h_m = 4 * V_m3 / (pi * d_m**2)
u_h = 4 / (pi * d_m**2) * np.sqrt(u_V**2 + (2 * V_m3/d_m * u_d)**2)

C_F = tau_s / R_ohm
u_C = 1/R_ohm * np.sqrt(u_tau**2 + (tau_s/R_ohm * u_R)**2)


plt.figure(figsize=(14, 4))


# Question 1
calibration_curve = plt.subplot(1, 3, 1)

calibration_curve.errorbar(
    x=h_m, xerr=u_h,
    y=C_F, yerr=u_C,
    capsize=4, c="blue", linestyle=''
)

calibration_curve.set_xlabel("h (m)")
calibration_curve.set_ylabel("C (F)")

calibration_curve.set_title("Courbe d'étalonnage")


# Question 2

NUMBER_OF_DRAWS = 5_000

a = []
b = []

for _ in range(NUMBER_OF_DRAWS):
    xs = rd.normal(h_m, scale=u_h)
    ys = rd.normal(C_F, scale=u_C)

    ai, bi = np.polyfit(x=xs, y=ys, deg=1)
    a.append(ai)
    b.append(bi)

a_mean = np.mean(a)
u_a_mean = np.std(a, ddof=1) / sqrt(len(a))
b_mean = np.mean(b)
u_b_mean = np.std(b, ddof=1) / sqrt(len(b))


# Question 3

curve = lambda x: a_mean*x + b_mean

curve_equation = (f"C = ({a_mean: .4e} ± {u_a_mean: .4e}) h +"
    + f" ({b_mean: .4e} ± {u_b_mean: .4e})")

calibration_curve.plot(
    [h_m[0], h_m[-1]],
    [curve(h_m[0]), curve(h_m[-1])],
    c="orange",
    label=curve_equation
)

calibration_curve.legend()


# Question 4

slope_hist = plt.subplot(1, 3, 2)
slope_hist.hist(a, bins =50, histtype="bar",
                edgecolor= "black", color="blue")

slope_hist.set_xlabel("a (F/m)")
slope_hist.set_title("Histogramme des pentes")


# Question 5

intercept_hist = plt.subplot(1, 3, 3)
intercept_hist.hist(b, bins =50 , histtype="bar",
                    edgecolor= "black", color="red")

intercept_hist.set_xlabel("b (F)")
intercept_hist.set_title("Histogramme de l'ordonnée à l'origine")


plt.show()
