 #set page(
    header: [Physique -- Travaux Numériques 2 #h(1fr)
        #smallcaps[Lux] Matthieu MP2I],
    numbering: "— 1/1 —"
)
// #set text(size: 14pt)

#show figure.caption: it => [
    #smallcaps[#it.supplement #it.counter.display(it.numbering)] --
    #it.body
]

#set heading(numbering: "1.1")

#show heading: it => block(
    if it.level == 2 {
        numbering(
            it.numbering,
            ..counter(heading)
                .at(it.location())
                .slice(1)
        ) + [. ]
    } + it.body
)


#align(center, [
    = TN2 -- Régression linéaire
])

#let res(formula) = rect($formula$, stroke: blue)
#let bigres(formula) = rect($ formula $, stroke: rgb("#000099"))

#rect(width: 100%, height: 13em)[
    #underline[Commentaire] :
]

== Courbe d'étalonnage

On veut créer une courbe d'étalonnage de la capacité $C$ du
condensateur  en fonction de la hauteur $h$ d'eau dans l'épouvette
gradué.

=== A - Hauteur d'eau

On modélise l'épouvette gradué par un cylindre, dont on connais le volume $V$
et le dimètre $d$ grâce à l'énoncé. On peut alors déterminer la hauteur
d'eau $h$ :
$ V = pi (d/2)^2 h ==> bigres(h = (4 V)/(pi d^2)) $
De plus, on détermine l'incertitude-type de $h$ grâce à la formule de
propagation des incertitudes :
$ u(h) = 4/pi sqrt( ((diff h)/(diff V) u(V))^2 + ((diff h)/(diff d) u(d))^2 ) 
    = 4/pi sqrt( (1/d^2 u(V))^2 + (-2 V 1/d^3 u(d))^2 ) $
$ ==> bigres(u(h) = 4/(pi d^2) sqrt( u(V)^2 + ((2V)/d u(d))^2 )) $


=== B - Capacité du condensateur

#table(
    columns: 2,
    stroke: none,
    align(center, figure(
        image("res/rc_serie.png", width: 16em),
        caption: [Circuit $R C$ série]
    )),
    [
        On s'intéresse à la charge du condensateur dans le circuit $R C$ série.
        On cheche à trouver l'expression du temps caractéristique :

        On utilise
        - la loi des mailles : $E = u_R + u_C$ ;
        - la loi d'Ohm sur $R$ : $u_R = R i$ ;
        - la caractéristique du condensateur $C$ : $i = C (dif u_C)/(dif t)$

        pour déterminer l'équation différentielle qui régit l'intensité $i$ :
    ]
)

$ E = i R + u_C 
    ==> R (dif i)/(dif t) + (dif u_C)/(dif t) = 0 
    ==> R C (dif i)/(dif t) + i = 0 
    ==> (dif i)/(dif t) + 1/(R C) i = 0 $
On obtient alors l'équation différentielle sous forme canonique, où l'on
identifie $tau$ ; et on peut donc déterminer la capacité $C$ en fontion de $tau$ et $R$ :
$ (dif i)/(dif t) + 1/tau i = 0 "où" tau = R C wide ==> bigres(C = tau/R) $

L'incertitude-type associé est calculée avec la formule de propagation des incertitudes :
$ u(C) = sqrt( ((diff C)/(diff tau) u(tau))^2 + ((diff C)/(diff R) u(R))^2 ) 
    = sqrt( (1/R u(tau))^2 + (-tau/R^2 u(R))^2 ) $
$ bigres(u(C) = 1/R sqrt( u(tau)^2 + (tau/R u(R))^2 )) $


=== C - Courbe
Le code écrit dans le script python réalise les calculs
précédents à l'aide des données de l'énoncé.
Le graphique de la capacité en fonction du
niveau d'eau est alors généré (voir @final).

#figure(
    image("res/final.png", width: 100%),
    caption: [Résultat final (voir `LuxMatthieuTN02.py`)]
) <final>


== Ajustement linéaire
Pour faire l'ajustement linéaire modélisant la courbe,
on utilise la méthode de Monte-Carlo :
+ on tire, pour chaque point de la courbe, un point
    aléatoire suivant une distribution normale ;
+ on utilise la fonction `np.polyfit` pour générer l'ajustement
    linéaire ;
+ on itère le procédé, et on réalise une étude statistique
    des ordonnées à l'origine et des pentes des droites :
    $ angle.l a angle.r = 1/N sum_(i = 1)^N a_i
        wide
        sigma_a = sqrt(1/(N - 1) sum_(i = 1)^N (a_i - angle.l a angle.r)^2)
        wide
        bigres(a = angle.l a angle.r plus.minus sigma_a/sqrt(n)) $


== Tracé de la droite
On peut alors tracer la droite avec les paramètres que l'on a déterminé (voir @final).


== Histogramme des pentes
Le script affiche l'histogramme des pentes de ces droites (voir @final).


== Histogramme des ordonnées à l'origine
Le script affiche également l'histogramme des ordonnées à l'origine de ces droites (voir @final).


#align(center, smallcaps[Fin])
